package kade.com.submission2.utils

import org.junit.Test

import org.junit.Assert.*
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify

class ApiRepoTest {
    //TODO : Unit Testing Koneksi ke Server 4
    @Test
    fun testDoRequest() {
        val apiRepository = mock(ApiRepo::class.java)
        val url = "https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=English%20Premier%20League"
        apiRepository.doRequest(url)
        verify(apiRepository).doRequest(url)
    }
}