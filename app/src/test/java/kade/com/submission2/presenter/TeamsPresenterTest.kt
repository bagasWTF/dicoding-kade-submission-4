package kade.com.submission2.presenter

import com.google.gson.Gson
import kade.com.submission2.data.Team
import kade.com.submission2.data.TeamResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.TSDBApi
import kade.com.submission2.utils.TestContextProvider
import kade.com.submission2.view.TeamsView
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TeamsPresenterTest {
//TODO : Unit Testing Koneksi ke Server 3

    @Mock
    private
    lateinit var view: TeamsView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepo

    @Mock
    private
    lateinit var presenter: TeamsPresenter


    @Before
    fun setUp() {
//        Log.d("test","masuk setup")
        MockitoAnnotations.initMocks(this)
        presenter = TeamsPresenter(view, apiRepository, gson, TestContextProvider())
    }
    @Test
    fun testGetTeamList() {
        val teams: List<Team> = mutableListOf()
        val response = TeamResponse(teams)
        val teamId = "English%20Premier%20League"
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TSDBApi.getTeams(teamId)),
                TeamResponse::class.java
        )).thenReturn(response)

        presenter.getTeamList(teamId)
        Mockito.verify(view).showLoading()
        Mockito.verify(view).showTeamList(teams)
        Mockito.verify(view).hideLoading()
    }
}