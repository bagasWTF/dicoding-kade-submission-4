package kade.com.submission2.presenter

import com.google.gson.Gson
import kade.com.submission2.data.Team
import kade.com.submission2.data.TeamResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.TSDBApi
import kade.com.submission2.utils.TestContextProvider
import kade.com.submission2.view.TeamDetailView
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class TeamDetailPresenterTest {
//TODO : Unit Testing Koneksi ke Server 1
    @Mock
    private
    lateinit var view: TeamDetailView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepo

    @Mock
    private
    lateinit var presenter: TeamDetailPresenter


    @Before
    fun setUp() {
//        Log.d("test","masuk setup")
        MockitoAnnotations.initMocks(this)
        presenter = TeamDetailPresenter(view, apiRepository, gson, TestContextProvider())
    }
    @Test
    fun testGetTeamDetail() {
        val teams: List<Team> = mutableListOf()
        val response = TeamResponse(teams)
        val teamId = "133932"
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TSDBApi.getTeamDetail(teamId)),
                TeamResponse::class.java
        )).thenReturn(response)

        presenter.getTeamDetail(teamId)
        Mockito.verify(view).showLoading()
        Mockito.verify(view).showTeamDetail(teams)
        Mockito.verify(view).hideLoading()
    }
}