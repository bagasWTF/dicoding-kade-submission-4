package kade.com.submission2.presenter

import com.google.gson.Gson
import kade.com.submission2.data.EventModel
import kade.com.submission2.data.EventResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.TSDBApi
import kade.com.submission2.utils.TestContextProvider
import kade.com.submission2.view.MainUI
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainPresenterTest {
    //TODO : Unit Testing Koneksi ke Server 5
    @Mock
    private
    lateinit var view: MainUI

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepo

    @Mock
    private
    lateinit var presenter: MainPresenter


    @Before
    fun setUp() {
//        Log.d("test","masuk setup")
        MockitoAnnotations.initMocks(this)
        presenter = MainPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun testGetLastMatchList() {
        val events: MutableList<EventModel> = mutableListOf()
        val response = EventResponse(events)
        val leagueId = "4328"
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TSDBApi.getLastMatch(leagueId)),
                EventResponse::class.java
        )).thenReturn(response)

        presenter.getLastMatchList(leagueId)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).updateEventMatch(events)
        Mockito.verify(view).hideLoading()
    }
    @Test
    fun testGetNextMatchList()  {
        val events: MutableList<EventModel> = mutableListOf()
        val response = EventResponse(events)
        val leagueId = "4328"
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TSDBApi.getNextMatch(leagueId)),
                EventResponse::class.java
        )).thenReturn(response)

        presenter.getNextMatchList(leagueId)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).updateEventMatch(events)
        Mockito.verify(view).hideLoading()
    }
}