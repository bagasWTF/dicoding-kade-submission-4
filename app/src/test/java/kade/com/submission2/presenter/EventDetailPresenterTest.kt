package kade.com.submission2.presenter

import android.net.Uri
import android.util.Log
import com.bumptech.glide.Glide
import com.google.gson.Gson
import kade.com.submission2.data.Team
import kade.com.submission2.data.TeamResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.TSDBApi
import kade.com.submission2.utils.TestContextProvider
import kade.com.submission2.view.EventDetailUI
import kade.com.submission2.view.TeamsView
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
//import org.junit.Before
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.runners.MockitoJUnitRunner

class EventDetailPresenterTest {
    //TODO : Unit Testing Koneksi ke Server 2
    @Mock
    private
    lateinit var view: EventDetailUI

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepo

    @Mock
    private
    lateinit var presenter: EventDetailPresenter

    @Before
    fun setUp() {
//        Log.d("test","masuk setup")
        MockitoAnnotations.initMocks(this)
        presenter = EventDetailPresenter(view, apiRepository, gson, TestContextProvider())
    }
    @Test
    fun testGetTeamAway() {
        val teams: List<Team> = mutableListOf()
        val response = TeamResponse(teams)
        val teamId = "133932"
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TSDBApi.getTeamDetail(teamId)),
                TeamResponse::class.java
        )).thenReturn(response)

        presenter.getTeamAway(teamId)
        Mockito.verify(view).showLoading()
        Mockito.verify(view).showTeamAway(teams)
        Mockito.verify(view).hideLoading()
    }

    @Test
    fun testGetTeamHome()  {
        val teams: MutableList<Team> = mutableListOf()
        val response = TeamResponse(teams)
        val teamId = "133932"
        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TSDBApi.getTeamDetail(teamId)),
                TeamResponse::class.java
        )).thenReturn(response)

        presenter.getTeamHome(teamId)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).showTeamHome(teams)
        Mockito.verify(view).hideLoading()
    }

}