package kade.com.submission2.testing

import org.junit.Test
import kade.com.submission2.testing.*
import org.junit.Assert.*
import java.text.SimpleDateFormat

class UtilsTest {
    private lateinit var utils: Utils
    @Test
    fun testToSimpleString() {
        utils = Utils()
        val dateFormat = SimpleDateFormat("MM/dd/yyyy")
        val date = dateFormat.parse("02/28/2018")
        assertEquals("Wed, 28 Feb 2018", utils.toSimpleString(date))
    }

}