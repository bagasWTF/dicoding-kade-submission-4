package kade.com.submission2

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import org.hamcrest.Matchers.not
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Before



@RunWith(AndroidJUnit4::class)
class HomeActivityTest {
    @Rule
    @JvmField var activityRule = ActivityTestRule(HomeActivity::class.java)

//    @Before
//    fun init() {
//        activityRule.getActivity()
//                .getSupportFragmentManager()
//                .beginTransaction()
//                .add(R.id.main_container,MainFragment())
//                .commit()
//    }
    @Test
    fun testRecyclerViewBehaviour() {

//        onView(withId(R.id.recView_id)).check(matches(not(isDisplayed())))

        onView(withId(R.id.recView_id)).check(matches(isCompletelyDisplayed()))
        onView(withId(R.id.recView_id)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(10))
        onView(withId(R.id.recView_id)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))
    }


}