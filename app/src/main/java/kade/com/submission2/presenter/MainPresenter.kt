package kade.com.submission2.presenter

import com.google.gson.Gson
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.data.EventResponse
import kade.com.submission2.utils.CoroutineContextProvider
import kade.com.submission2.view.MainUI
import kade.com.submission2.utils.TSDBApi
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class MainPresenter(private val view: MainUI,
                    private val apiRepository: ApiRepo,
                    private val gson: Gson,
                    private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getNextMatchList(leagueId: String?) {
        view.showLoading()

        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TSDBApi.getNextMatch(leagueId)),
                        EventResponse::class.java
                )
            }
            view.updateEventMatch(data.await().events)
            view.hideLoading()
        }
//        doAsync {
//            val data = gson.fromJson(apiRepository
//                    .doRequest(TSDBApi.getNextMatch(leagueId)), EventResponse::class.java
//            )
//
//            uiThread {
//                view.hideLoading()
//                view.updateEventMatch(data.events)
//            }
//        }
    }
    fun getLastMatchList(leagueId: String?) {
        view.showLoading()

        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TSDBApi.getLastMatch(leagueId)),
                        EventResponse::class.java
                )
            }
            view.updateEventMatch(data.await().events)
            view.hideLoading()
        }

//        doAsync {
//            val data = gson.fromJson(apiRepository
//                    .doRequest(TSDBApi.getLastMatch(leagueId)), EventResponse::class.java
//            )
//
//            uiThread {
//                view.hideLoading()
//                view.updateEventMatch(data.events)
//            }
//        }
    }
}