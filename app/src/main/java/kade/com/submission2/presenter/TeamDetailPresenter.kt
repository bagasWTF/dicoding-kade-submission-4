package kade.com.submission2.presenter

import com.google.gson.Gson
import kade.com.submission2.view.TeamDetailView
import kade.com.submission2.data.TeamResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.CoroutineContextProvider
import kade.com.submission2.utils.TSDBApi
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TeamDetailPresenter(private val view: TeamDetailView,
                          private val apiRepository: ApiRepo,
                          private val gson: Gson,
                          private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getTeamDetail(teamId: String) {
        view.showLoading()

        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TSDBApi.getTeamDetail(teamId)),
                        TeamResponse::class.java
                )
            }
            view.showTeamDetail(data.await().teams)
            view.hideLoading()
        }
//        doAsync {
//            val data = gson.fromJson(apiRepository
//                    .doRequest(TSDBApi.getTeamDetail(teamId)),
//                    TeamResponse::class.java
//            )
//
//            uiThread {
//                view.hideLoading()
//                view.showTeamDetail(data.teams)
//            }
//        }
    }
}