package kade.com.submission2.presenter

import android.util.Log
import com.google.gson.Gson
import kade.com.submission2.view.TeamsView
import kade.com.submission2.data.TeamResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.utils.CoroutineContextProvider
import kade.com.submission2.utils.TSDBApi
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class TeamsPresenter (private val view: TeamsView,
                      private val apiRepository: ApiRepo,
                      private val gson: Gson,
                      private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getTeamList(league: String?) {
        view.showLoading()

        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TSDBApi.getTeams(league)),
                        TeamResponse::class.java
                )
            }
            view.showTeamList(data.await().teams)
            view.hideLoading()
        }

//        doAsync {
//            val data = gson.fromJson(apiRepository
//                    .doRequest(TSDBApi.getTeams(league)),
//                    TeamResponse::class.java
//            )
//
//            uiThread {
//                view.hideLoading()
//                Log.d("UIThread","output : "+data)
//                view.showTeamList(data.teams)
//            }
//        }
    }
}