package kade.com.submission2.presenter

import com.google.gson.Gson
import kade.com.submission2.data.EventResponse
import kade.com.submission2.utils.ApiRepo
import kade.com.submission2.view.EventDetailUI
import kade.com.submission2.utils.TSDBApi
import kade.com.submission2.data.TeamResponse
import kade.com.submission2.utils.CoroutineContextProvider
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class EventDetailPresenter(private val view: EventDetailUI,
                           private val apiRepository: ApiRepo,
                           private val gson: Gson,
                           private val context: CoroutineContextProvider = CoroutineContextProvider()) {
    fun getTeamAway(teamAwayId: String?) {
        view.showLoading()

        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TSDBApi.getTeamDetail(teamAwayId)),
                        TeamResponse::class.java
                )
            }
            view.showTeamAway(data.await().teams)
            view.hideLoading()
        }
    }

    fun getTeamHome(teamHomeId: String?) {
        view.showLoading()

        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TSDBApi.getTeamDetail(teamHomeId)),
                        TeamResponse::class.java
                )
            }
            view.showTeamHome(data.await().teams)
            view.hideLoading()
        }
    }

}