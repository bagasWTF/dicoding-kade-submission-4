package kade.com.submission2.view

import kade.com.submission2.data.Team


interface TeamsView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
}