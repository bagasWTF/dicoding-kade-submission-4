package kade.com.submission2.view

import kade.com.submission2.data.EventModel
import kade.com.submission2.data.Team

interface EventDetailUI {
    fun showTeamAway(awayTeam: List<Team>)
    fun showTeamHome(homeTeam: List<Team>)
//    fun showTeamAwayDetail(awayTeamDet: List<EventModel>?)
//    fun showTeamHomeDetail(homeTeamDet: List<EventModel>?)
    fun showLoading()
    fun hideLoading()
}