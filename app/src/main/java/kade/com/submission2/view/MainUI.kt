package kade.com.submission2.view

import kade.com.submission2.data.EventModel

interface MainUI {
    fun showLoading()
    fun hideLoading()
    fun updateEventMatch(data: List<EventModel>)
}