package kade.com.submission2.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import kade.com.submission2.R
import kade.com.submission2.data.Team
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView
import org.jetbrains.anko.sdk25.coroutines.onClick

class TeamsAdapter (private val context: Context?, private val teams: List<Team>, private val listener: (Team) -> Unit): RecyclerView.Adapter<NextMatchViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NextMatchViewHolder {
        return NextMatchViewHolder(ItemListUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun getItemCount(): Int = teams.size

    override fun onBindViewHolder(holder: NextMatchViewHolder, position: Int) {
        holder.bindItem(teams[position], listener)
    }



}

class NextMatchViewHolder(view: View) : RecyclerView.ViewHolder(view){
    var tvTitle: ImageView
    var tvYear: TextView

    init {
        tvTitle = itemView.findViewById(ItemListUI.tvTitleId)
        tvYear = itemView.findViewById(ItemListUI.tvYearId)
    }
    fun bindItem(items: Team, listener: (Team) -> Unit) {
        tvYear.text= items.teamName
        Glide.with(itemView.context).load(items.teamBadge).into(tvTitle)
        itemView.onClick { listener(items) }
    }
}
class ItemListUI : AnkoComponent<ViewGroup> {

    companion object {
        val tvTitleId = 1
        val tvYearId = 2
    }

    override fun createView(ui: AnkoContext<ViewGroup>): View = with(ui){
        linearLayout {
            lparams(matchParent, wrapContent)
            padding = dip(16)

            imageView {
                id = tvTitleId
                layoutParams = LinearLayout.LayoutParams(dip(64),dip(64))


            }

            verticalLayout {

                lparams(wrapContent, matchParent)
                gravity = Gravity.CENTER
                leftPadding = dip(16)
                textView {
                    id = tvYearId
                    layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                    text = "2009"
                    textSize = 18f
                }
            }
        }
    }
}
class NextMatchUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            frameLayout {
                lparams(matchParent, wrapContent)
                cardView {
                    layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT).apply {
                        leftMargin = dip(10)
                        rightMargin = dip(10)
                        topMargin = dip(5)
                        bottomMargin = dip(5)
                    }
                    background.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
                    radius = dip(8).toFloat()
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)
                        padding = dip(16)
                        orientation = LinearLayout.VERTICAL

//                imageView {
//                    id = R.id.team_badge
//                }.lparams{
//                    height = dip(50)
//                    width = dip(50)
//                }

                        textView {
                            id = R.id.event_date
                            textSize = 16f
                            textAlignment = View.TEXT_ALIGNMENT_CENTER
                        }
                        linearLayout{
                            lparams(matchParent, wrapContent)
                            orientation = LinearLayout.HORIZONTAL

                            textView {
                                id = R.id.team_home_name
                                textSize = 16f
                                textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            }.lparams(0, wrapContent){
                                weight = 2f
                                //                        margin = dip(15)
                            }.setTypeface(Typeface.DEFAULT_BOLD)

                            linearLayout {
                                lparams(0, wrapContent){
                                    weight = 2f
                                    padding = dip(8)
                                    orientation = LinearLayout.HORIZONTAL
                                }
                                textView{
                                    id = R.id.team_home_score
                                    textSize = 14f
                                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                                }.lparams(0, wrapContent){
                                    weight = 1f
                                    //                            margin = dip(15)
                                }
                                textView{
                                    text = "VS"
                                    textSize = 14f
                                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                                }.lparams(0, wrapContent){
                                    margin = dip(5)
                                    weight = 1f
                                }.setTypeface(Typeface.DEFAULT_BOLD)
                                textView{
                                    id = R.id.team_away_score
                                    textSize = 14f
                                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                                }.lparams(0, wrapContent){
                                    weight = 1f
                                    //                            margin = dip(15)
                                }
                            }

                            textView {
                                id = R.id.team_away_name
                                textSize = 16f
                            }.lparams(0, wrapContent){
                                weight = 2f
                                //                        margin = dip(15)
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }

                    }
                }
            }
        }
    }

}