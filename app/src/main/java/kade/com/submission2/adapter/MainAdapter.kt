package kade.com.submission2.adapter

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import kade.com.submission2.R
import kade.com.submission2.R.id.*
import kade.com.submission2.data.EventModel
import org.jetbrains.anko.*
import org.jetbrains.anko.cardview.v7.cardView

class MainAdapter (private val context: Context, private val teams: List<EventModel>, private val listener: (EventModel) -> Unit): RecyclerView.Adapter<EventViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
            return EventViewHolder(NextMatchUI().createView(AnkoContext.create(parent.context, parent)))
        }

        override fun getItemCount(): Int = teams.size

        override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
            holder.bindItem(teams[position], listener)
        }



}

class EventViewHolder(view: View) : RecyclerView.ViewHolder(view){
//    private val teamBadge: ImageView = view.find(team_badge)
    private val teamAwayName: TextView = view.find(team_away_name)
    private val teamHomeName: TextView = view.find(team_home_name)
    private val teamAwayScore: TextView = view.find(team_away_score)
    private val teamHomeScore: TextView = view.find(team_home_score)
    private val eventDate: TextView = view.find(event_date)
    fun bindItem(nMatch: EventModel, listener: (EventModel) -> Unit) {
//        Picasso.get().load(nMatch.).into(teamBadge)
        teamAwayName.text = checkString(nMatch.teamAwayName)
        teamAwayScore.text = nMatch.teamAwayScore
        teamHomeName.text = checkString(nMatch.teamHomeName)
        teamHomeScore.text = nMatch.teamHomeScore
        eventDate.text = dateformat(nMatch.eventDate)
        itemView.setOnClickListener {
            listener(nMatch)
        }
    }
    fun checkString(data : String?) : String{
        var dataInput : String = data.toString()
        if (dataInput.length > 13){
            dataInput = dataInput.substring(0,12) + ".."
        }
        return dataInput
    } fun dateformat(data: String?) : String {
        var dataInput : String = data.toString()
        var arrData = dataInput.split("-")
        var bulan : String?
        when(arrData[1]){
            "01"-> bulan = "Jan"
            "02"-> bulan = "Feb"
            "03"-> bulan = "Mar"
            "04"-> bulan = "Apr"
            "05"-> bulan = "Mei"
            "06"-> bulan = "Jun"
            "07"-> bulan = "Jul"
            "08"-> bulan = "Aug"
            "09"-> bulan = "Sep"
            "10"-> bulan = "Oct"
            "11"-> bulan = "Nov"
            "12"-> bulan = "Dec"
            else->{
                bulan = "-"
            }
        }
        return arrData[2]+" "+bulan+" "+arrData[0]
    }
}
class EventUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            frameLayout {
                lparams(matchParent, wrapContent)
                cardView {
                    layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT).apply {
                        leftMargin = dip(10)
                        rightMargin = dip(10)
                        topMargin = dip(5)
                        bottomMargin = dip(5)
                    }
                        background.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP)
                        radius = dip(8).toFloat()
                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)
                        padding = dip(16)
                        orientation = LinearLayout.VERTICAL

//                imageView {
//                    id = R.id.team_badge
//                }.lparams{
//                    height = dip(50)
//                    width = dip(50)
//                }

                        textView {
                            id = R.id.event_date
                            textSize = 16f
                            textAlignment = View.TEXT_ALIGNMENT_CENTER
                        }
                        linearLayout{
                            lparams(matchParent, wrapContent)
                            orientation = LinearLayout.HORIZONTAL

                            textView {
                                id = R.id.team_home_name
                                textSize = 16f
                                textAlignment = View.TEXT_ALIGNMENT_TEXT_END
                            }.lparams(0, wrapContent){
                                weight = 2f
                                //                        margin = dip(15)
                            }.setTypeface(Typeface.DEFAULT_BOLD)

                            linearLayout {
                                lparams(0, wrapContent){
                                    weight = 2f
                                    padding = dip(8)
                                    orientation = LinearLayout.HORIZONTAL
                                }
                                textView{
                                    id = R.id.team_home_score
                                    textSize = 14f
                                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                                }.lparams(0, wrapContent){
                                    weight = 1f
                                    //                            margin = dip(15)
                                }
                                textView{
                                    text = "VS"
                                    textSize = 14f
                                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                                }.lparams(0, wrapContent){
                                    margin = dip(5)
                                    weight = 1f
                                }.setTypeface(Typeface.DEFAULT_BOLD)
                                textView{
                                    id = R.id.team_away_score
                                    textSize = 14f
                                    textAlignment = View.TEXT_ALIGNMENT_CENTER
                                }.lparams(0, wrapContent){
                                    weight = 1f
                                    //                            margin = dip(15)
                                }
                            }

                            textView {
                                id = R.id.team_away_name
                                textSize = 16f
                            }.lparams(0, wrapContent){
                                weight = 2f
                                //                        margin = dip(15)
                            }.setTypeface(Typeface.DEFAULT_BOLD)
                        }

                    }
                }
            }
        }
    }

}