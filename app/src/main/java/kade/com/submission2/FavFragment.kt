package kade.com.submission2

import android.support.v4.app.FragmentTransaction
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import android.content.Context
import android.database.sqlite.SQLiteException
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import kade.com.submission1.FavoriteTeamsFragment
import kade.com.submission2.adapter.FavoriteEventsAdapter
import kade.com.submission2.sqlite.FavEvents
import kade.com.submission2.sqlite.Favorite
import kade.com.submission2.sqlite.database
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.support.v4.swipeRefreshLayout

class FavFragment : Fragment(), AnkoComponent<Context> {
    private lateinit var spinner: Spinner
    private lateinit var spinnerItem : String
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = "Favorites"
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
//        loadFavoritesTeamFragment(savedInstanceState)
        val spinnerItems = resources.getStringArray(R.array.favArray)
        val spinnerAdapter = ArrayAdapter(ctx, android.R.layout.simple_spinner_dropdown_item, spinnerItems)
        spinner.adapter = spinnerAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                spinnerItem = spinner.selectedItem.toString()
                when(spinnerItem){
                    spinnerItems[0] -> {
                        loadFavoritesEventsFragment(savedInstanceState)
                    }
                    spinnerItems[1] -> {
                        loadFavoritesTeamFragment(savedInstanceState)
                    }
                }

            }
            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
//
//        fragment = FavoriteEventsFragment();
//        childFragmentManager
//                .beginTransaction()
//                .add(R.id.fav_container_id,fragment).commit()
    }
    override fun createView(ui: AnkoContext<Context>): View = with(ui){
        verticalLayout {
            spinner = spinner ()
            frameLayout {
                id = R.id.fav_container_id
                lparams(matchParent, matchParent)
            }.lparams(matchParent, matchParent)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }
    private fun loadFavoritesTeamFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            childFragmentManager
                    .beginTransaction()
                    .replace(R.id.fav_container_id, FavoriteTeamsFragment())
                    .commit()
        } else {
                childFragmentManager
                        .beginTransaction()
                        .add(R.id.fav_container_id, FavoriteTeamsFragment())
                        .commit()
        }
    }

    private fun loadFavoritesEventsFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            childFragmentManager
                    .beginTransaction()
                    .replace(R.id.fav_container_id, FavoriteEventsFragment())
                    .commit()
        } else {
                childFragmentManager
                        .beginTransaction()
                        .add(R.id.fav_container_id, FavoriteEventsFragment())
                        .commit()
        }
    }
}