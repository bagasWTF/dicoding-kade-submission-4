package kade.com.submission2.testing

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

class Utils {

    @SuppressLint("SimpleDateFormat")
    fun toSimpleString(date: Date?): String? = with(date ?: Date()) {
        SimpleDateFormat("EEE, dd MMM yyy").format(this)
    }
}