package kade.com.submission2.data

import com.google.gson.annotations.SerializedName

data class EventModel(
        @SerializedName("idEvent")
        var eventId: String? = null,
        @SerializedName("strEvent")
        var eventName: String? = null,
        @SerializedName("dateEvent")
        var eventDate: String? = null,

        //TEAM HOME
        @SerializedName("idHomeTeam")
        var teamHomeId: String? = null,
        @SerializedName("strHomeTeam")
        var teamHomeName: String? = null,
        @SerializedName("intHomeScore")
        var teamHomeScore: String? = null,
        @SerializedName("intHomeShots")
        var teamHomeShots: String? = null,
        @SerializedName("strHomeLineupGoalkeeper")
        var teamHomeGoalKeeper: String? = null,
        @SerializedName("strHomeLineupDefense")
        var teamHomeDefense: String? = null,
        @SerializedName("strHomeLineupMidfield")
        var teamHomeMidField: String? = null,
        @SerializedName("strHomeLineupForward")
        var teamHomeForward: String? = null,
        @SerializedName("strHomeLineupSubstitutes")
        var teamHomeSubstitutes: String? = null,

        //TEAM AWAY
        @SerializedName("idAwayTeam")
        var teamAwayId: String? = null,
        @SerializedName("strAwayTeam")
        var teamAwayName: String? = null,
        @SerializedName("intAwayScore")
        var teamAwayScore: String? = null,
        @SerializedName("intAwayShots")
        var teamAwayShots: String? = null,
        @SerializedName("strAwayLineupGoalkeeper")
        var teamAwayGoalKeeper: String? = null,
        @SerializedName("strAwayLineupDefense")
        var teamAwayDefense: String? = null,
        @SerializedName("strAwayLineupMidfield")
        var teamAwayMidField: String? = null,
        @SerializedName("strAwayLineupForward")
        var teamAwayForward: String? = null,
        @SerializedName("strAwayLineupSubstitutes")
        var teamAwaySubstitutes: String? = null

)