package kade.com.submission2.data
//
data class TeamResponse(
        val teams: List<Team>)