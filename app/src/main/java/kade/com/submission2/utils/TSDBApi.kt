package kade.com.submission2.utils

import android.net.Uri
import kade.com.submission2.BuildConfig

object TSDBApi {
    fun getTeams(leagueName: String?): String{
        return BuildConfig.BASE_URL +
                "api/v1/json/${BuildConfig.TSDB_API_KEY}" +
                "/search_all_teams.php?l=" + leagueName
    }
    fun getTeamDetail(teamId: String?): String{
        return BuildConfig.BASE_URL +
                "api/v1/json/${BuildConfig.TSDB_API_KEY}" +
                "/lookupteam.php?id=" + teamId
    }
    fun getNextMatch(leagueId: String?): String{
        return BuildConfig.BASE_URL +
                "api/v1/json/${BuildConfig.TSDB_API_KEY}" +
                "/eventsnextleague.php?id=" + leagueId
    }
    fun getLastMatch(leagueId: String?): String{
        return BuildConfig.BASE_URL +
                "api/v1/json/${BuildConfig.TSDB_API_KEY}" +
                "/eventspastleague.php?id=" + leagueId
    }
}